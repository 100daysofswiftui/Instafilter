# Instafilter

Instafilter is an app that lets the user import photos from their library, then modify them using various image effects. 

This app was developed as part of the [100 Days of SwiftUI](https://www.hackingwithswift.com/books/ios-swiftui/instafilter-introduction) course from Hacking with Swift.

## Topics
The following topics were handled in this project:
- State changes
- Showing confirmation dialogs
- How to manipulate images using Core Image
- How to handling missing content in your app
- Loading photos
- Sharing data
- Asking for app reviews
- PhotosPicker
- Filtering images using Core Image
- Confirmation dialogs
- ShareLink