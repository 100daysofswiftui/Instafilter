//
//  InstafilterApp.swift
//  Instafilter
//
//  Created by Pascal Hintze on 08.03.2024.
//

import SwiftUI

@main
struct InstafilterApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
